from typing import Iterable, Optional

from loguru import logger

from constants import logins_users


class YoutrackIssueWorkItem:
    def __init__(self, issue: str, author: str = None, duration: int = 0):
        self._issue = issue
        self._author = author
        self._duration = duration

    def __str__(self):
        return f"Задача: {self.issue}, исполнитель: {self.author}, время: {self.duration}"

    def __key(self):
        return self.issue, self.author, self.duration

    def __hash__(self):
        return hash((self.issue, self.author, self.duration))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    @property
    def issue(self):
        return self._issue

    @issue.setter
    def issue(self, value):
        self._issue = value

    @property
    def author(self):
        return self._author

    @author.setter
    def author(self, value):
        self._author = value

    @property
    def duration(self):
        return self._duration

    @duration.setter
    def duration(self, value):
        self._duration = value

    def __add_key(self):
        return self.issue, self.author

    def __add__(self, other):
        if not isinstance(other, self.__class__) or self.__add_key() != other.__add_key():
            return NotImplemented
        return YoutrackIssueWorkItem(self.issue, self.author, self.duration + other.duration)

    def __iadd__(self, other):
        if not isinstance(other, self.__class__) or self.__add_key() != other.__add_key():
            return NotImplemented
        return YoutrackIssueWorkItem(self.issue, self.author, self.duration + other.duration)


class YoutrackIssue:
    def __init__(self, issue: str, assignee: Optional[str] = None, assignees_project: Optional[Iterable[str]] = None):
        if assignees_project is None:
            assignees_project = []
        self._issue = issue
        self._assignee = assignee
        self._assignees_project: list[str] = assignees_project

    def __str__(self):
        return f"Задача: {self.issue}, исполнитель: {self.assignee}\n" \
               f"Исполнители: {self.all_users}"

    @property
    def issue(self):
        return self._issue

    @issue.setter
    def issue(self, value):
        self._issue = value

    @property
    def assignee(self):
        return self._assignee

    @assignee.setter
    def assignee(self, value):
        self._assignee = value

    @property
    def assignees_project(self):
        return self._assignees_project

    @assignees_project.setter
    def assignees_project(self, value):
        self._assignees_project = value

    @property
    def all_users(self) -> Iterable[str]:
        if self.assignees_project:
            users: list[str] = [self.assignee, *self.assignees_project]
        else:
            users: list[str] = [self.assignee]
        return users

    def __key(self):
        return self.issue, self.all_users

    def __hash__(self):
        return hash((self.issue, self.all_users))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented


class YoutrackInfo:
    def __init__(self, issue: str, assignee: str = None, user_time: dict[str, int] = None):
        if user_time is None:
            user_time = dict()
        self._issue: str = issue
        self._assignee: str = assignee
        self._user_time: dict[str, int] = user_time

    def __str__(self):
        return f"Задача {self.issue}, исполнитель: {self.assignee};\n" \
               f"Затраченное время по исполнителям:\n" \
               f"{self._user_time_presentation()}\n"

    def _user_time_presentation(self):
        return "\n".join([f"{logins_users[k]}: {v} минут" for k, v in self.user_time.items()])

    @property
    def issue(self):
        return self._issue

    @issue.setter
    def issue(self, value):
        self._issue = value

    @property
    def assignee(self):
        return self._assignee

    @assignee.setter
    def assignee(self, value):
        self._assignee = value

    @property
    def user_time(self):
        return self._user_time

    @user_time.setter
    def user_time(self, value):
        self._user_time = value

    def __getitem__(self, key):
        if key in self.user_time.keys():
            return self.user_time.get(key)
        return

    def __setitem__(self, key, value):
        self.user_time.__setitem__(key, value)


class YoutrackInfoFactory:
    # _instance = None
    #
    # def __new__(
    #         cls,
    #         issues: Iterable[YoutrackIssue] = None,
    #         issue_work_items: Iterable[YoutrackIssueWorkItem] = None,
    #         *args, **kwargs):
    #     if cls._instance is None:
    #         if issues is None:
    #             issues = {}
    #         if issue_work_items is None:
    #             issue_work_items = {}
    #         cls._issues: dict[str, YoutrackIssue] = {issue.issue: issue for issue in issues}
    #         cls._issue_work_items: dict[str, YoutrackIssueWorkItem] = {
    #             issue_work_item.issue: issue_work_item for issue_work_item in issue_work_items}
    #
    #         cls._youtrack_info: dict[str, YoutrackInfo] = dict()
    #     return cls._instance

    def __init__(
            self,
            issues: Iterable[YoutrackIssue] = None,
            issue_work_items: Iterable[YoutrackIssueWorkItem] = None):
        if issues is None:
            issues = {}
        if issue_work_items is None:
            issue_work_items = {}
        self._issues: dict[str, YoutrackIssue] = {issue.issue: issue for issue in issues}
        self._issue_work_items: dict[str, YoutrackIssueWorkItem] = {
            issue_work_item.issue: issue_work_item for issue_work_item in issue_work_items}

        self._youtrack_info: dict[str, YoutrackInfo] = dict()

    # def __call__(self, *args, **kwargs):
    #     return self._instance

    @property
    def issues(self):
        return self._issues

    @issues.setter
    def issues(self, value):
        self.__class__._issues = value

    @property
    def issue_work_items(self):
        return self._issue_work_items

    @issue_work_items.setter
    def issue_work_items(self, value):
        self.__class__._issue_work_items = value

    @issue_work_items.deleter
    def issue_work_items(self):
        for _ in self._issue_work_items.items():
            del _
        del self._issue_work_items



    @property
    def issue_names(self) -> list[str]:
        return list(self.issues.keys())

    def issue_work_items_issue(self, issue: str) -> list[YoutrackIssueWorkItem]:
        return list(filter(lambda x: x.issue == issue, self.issue_work_items.values()))

    def generate_youtrack_info_issue(self, youtrack_info: YoutrackInfo):
        user_time: dict[str, int] = dict()
        issue: str = youtrack_info.issue
        issue_work_items: list[YoutrackIssueWorkItem] = self.issue_work_items_issue(issue)
        for item in issue_work_items:
            logger.info("issue_work_items")
            logger.info(str(item))
        youtrack_issue: YoutrackIssue = self.issues.get(issue)
        logger.info(str(youtrack_issue))
        for user in youtrack_issue.all_users:
            logger.info(f"user = {user}")
            user_time[user] = 0
        for issue_work_item in issue_work_items:
            if issue_work_item.author not in user_time.keys():
                user_time[issue_work_item.author] = 0
            user_time[issue_work_item.author] += issue_work_item.duration
            logger.info(f"user = {issue_work_item.author}, time = {user_time[issue_work_item.author]}")
        youtrack_info.assignee = youtrack_issue.assignee
        youtrack_info.user_time = user_time
        return youtrack_info

    def generate_youtrack_info(self):
        return list(map(lambda x: self.generate_youtrack_placeholder(x), self.issue_names))

    def generate_youtrack_placeholder(self, issue: str):
        youtrack_info: YoutrackInfo = YoutrackInfo(issue)
        self._youtrack_info[youtrack_info.issue] = youtrack_info
        return youtrack_info