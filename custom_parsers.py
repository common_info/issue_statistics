import json
from typing import Union, Optional, NamedTuple, Iterable

from loguru import logger

from .constants import logins_users, _logins


class TimeConverter:
    _instance = None
    _conversion_table: dict[str, int] = {
        "hours": 60,
        "days": 8,
        "weeks": 5
    }

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def convert_time(cls, time: int):
        if time < cls._conversion_table["hours"]:
            return f"{time} мин"
        hours, minutes = divmod(time, cls._conversion_table["hours"])
        if hours < cls._conversion_table["days"]:
            return f"{hours} ч {minutes} мин"
        days, hours = divmod(hours, cls._conversion_table["days"])
        if days < cls._conversion_table["weeks"]:
            return f"{days} д {hours} ч {minutes} мин"
        weeks, days = divmod(days, cls._conversion_table["weeks"])
        return f"{weeks} н {days} д {hours} ч {minutes} мин"


class YoutrackIssue(NamedTuple):
    issue: str = None
    summary: str = None
    assignee: str = None
    assignees_project: list[str] = []

    def __repr__(self):
        if self.assignees_project:
            _assignees: str = ", ".join(self.assignees_project)
            _assignees_string: str = f", {_assignees}"
        else:
            _assignees_string: str = ""
        return f"<{self.__class__.__name__}({self.issue}, {self.summary}, {self.assignee}{_assignees_string})>"


class YoutrackIssueWorkItem(NamedTuple):
    issue: str = None
    login: str = None
    spent_time: int = 0

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.issue}, {self.login}, {self.spent_time})>"


class YoutrackInfoRepresentation(NamedTuple):
    issue: str
    summary: str
    assignee: str
    user_time: dict[str, int] = dict()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.issue}, {self.summary}, {self.assignee}, {self._user_time_string})>"

    def __str__(self):
        return f"Задача {self.issue}: {self.summary}, исполнитель: {self.assignee}\n{self._user_time_string}"

    @property
    def _user_time_string(self):
        return "\n".join([f"{k}: {v}" for k, v in self.user_time.items()])

    @property
    def _repr_issue(self):
        return f'Задача: {self.issue}, \"{self.summary}\";'

    @property
    def _repr_assignee(self):
        try:
            result = f"исполнитель: {logins_users[self.assignee]};"
        except KeyError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return f"исполнитель: {self.assignee};"
        else:
            return result

    @property
    def _repr_user_time(self):
        return "\n".join(
            [f"{logins_users[login]}: {TimeConverter.convert_time(time)}"
             for login, time in self.user_time.items()]
        )

    def repr(self):
        return f"{self._repr_issue} {self._repr_assignee}\n{self._repr_user_time}"


class CustomParser:
    def __init__(self, response_string_json: str = None):
        self._response_string_json: str = response_string_json

    @property
    def dict_response(self) -> list[dict[str, Union[str, int, dict, list]]]:
        return json.loads(self._response_string_json)

    def __iter__(self):
        return iter(self.dict_response)


class IssueResponseParser(CustomParser):
    def get_issue_id(self):
        return [issue_parameters["idReadable"] for issue_parameters in self]


class AssigneesResponseParser(CustomParser):
    def get_youtrack_issues(self) -> list[YoutrackIssue]:
        youtrack_issues: list[YoutrackIssue] = []
        for item in self:
            issue: str = item["idReadable"]
            summary: str = item["summary"]
            assignee: Optional[str] = None
            assignees_project: Optional[list[str]] = None
            for custom_field in item["customFields"]:
                # noinspection PyTypeChecker
                if custom_field["name"] == "Assignee":
                    # noinspection PyTypeChecker
                    assignee: str = custom_field["value"]["login"]
                elif custom_field["name"].startswith("Исполнители_"):
                    # noinspection PyTypeChecker
                    assignees_project: list[str] = [element["login"] for element in custom_field["value"]]
                else:
                    continue
            if issue is None:
                continue
            youtrack_issue: YoutrackIssue = YoutrackIssue(issue, summary, assignee, assignees_project)
            youtrack_issues.append(youtrack_issue)
        return youtrack_issues


class IssueWorkItemParser(CustomParser):
    def get_issue_work_items(self) -> list[YoutrackIssueWorkItem]:
        issue_work_items: list[YoutrackIssueWorkItem] = []
        for item in self:
            issue: str = item["issue"]["idReadable"]
            duration: int = item["duration"]["minutes"]
            author: str = item["author"]["login"]
            if author in _logins:
                issue_work_item: YoutrackIssueWorkItem = YoutrackIssueWorkItem(issue, author, duration)
                issue_work_items.append(issue_work_item)
            else:
                continue
        return issue_work_items


class YoutrackInfo:
    def __init__(
            self,
            youtrack_issues: Iterable[YoutrackIssue] = None,
            youtrack_issue_work_items: Iterable[YoutrackIssueWorkItem] = None):
        if youtrack_issues is None:
            _issues_dict: dict[str, YoutrackIssue] = dict()
        else:
            _issues_dict: dict[str, YoutrackIssue] = {issue.issue: issue for issue in youtrack_issues}
        _items_dict: dict[str, list[YoutrackIssueWorkItem]] = dict()

        self._issues_dict: dict[str, YoutrackIssue] = _issues_dict
        self._items_dict: dict[str, list[YoutrackIssueWorkItem]] = self._get_items_dict(youtrack_issue_work_items)

    @staticmethod
    def _get_items_dict(issue_work_items: Iterable[YoutrackIssueWorkItem]):
        issues = list(set(item.issue for item in issue_work_items))
        _dict_items: dict[str, list[YoutrackIssueWorkItem]] = dict()
        for issue in issues:
            _dict_items[issue] = []
            items = list(filter(lambda x: x.issue == issue, issue_work_items))
            _dict_items[issue].extend(items)
        return _dict_items

    def join_youtrack_items(self, issue: str) -> Optional[dict[str, int]]:
        youtrack_items: list[YoutrackIssueWorkItem] = self._items_dict.get(issue, [])
        # logger.error(f"youtrack_items = {youtrack_items}")
        user_time = dict()
        if not youtrack_items:
            return user_time
        for item in youtrack_items:
            if item.login not in user_time.keys():
                user_time[item.login] = 0
            user_time[item.login] += item.spent_time
        return user_time

    def prepare_youtrack_info_representation(self, issue: str):
        youtrack_issue: YoutrackIssue = self._issues_dict.get(issue)
        if youtrack_issue is None:
            return YoutrackInfoRepresentation("None", "None", "None")
        summary: str = youtrack_issue.summary
        assignee: str = youtrack_issue.assignee
        assignees_project: list[str] = youtrack_issue.assignees_project
        user_time: dict[str, int] = self.join_youtrack_items(issue)
        if assignee not in user_time.keys():
            user_time[assignee] = 0
        if assignees_project:
            for _ in assignees_project:
                if _ not in user_time.keys():
                    user_time[_] = 0
        return YoutrackInfoRepresentation(issue, summary, assignee, user_time)
