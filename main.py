import json

from loguru import logger

from init_logger import complete_configure_custom_logging
from .constants import CustomHTTPMethod, CustomScheme, CustomPort, auth_headers
from .custom_parsers import YoutrackIssue, YoutrackIssueWorkItem, YoutrackInfoRepresentation, \
    IssueResponseParser, AssigneesResponseParser, IssueWorkItemParser, YoutrackInfo
from .custom_requests import CustomHTTPRequest, CustomPreparedRequest, CustomHTTPClient

_protocol = None


def set_default(is_secure: bool = True):
    if is_secure:
        globals()["_protocol"] = "HTTPS"
    else:
        globals()["_protocol"] = "HTTP"


def send_custom_request_spec(
        custom_http_request: CustomHTTPRequest,
        *,
        method: CustomHTTPMethod = CustomHTTPMethod.NONE,
        data: bytes = None,
        headers: dict[str, str] = None):
    custom_prepared_request: CustomPreparedRequest
    custom_prepared_request = CustomPreparedRequest.from_custom_request(
        custom_http_request=custom_http_request,
        method=method.value,
        data=data,
        headers=headers)
    custom_http_client: CustomHTTPClient = CustomHTTPClient(custom_prepared_request)
    return custom_http_client.get_response()


def send_custom_request(
        *,
        scheme: CustomScheme = CustomScheme.NONE,
        web_hook: str = None,
        host: str = None,
        port: CustomPort = CustomPort.NONE,
        params: tuple[tuple[str, str]] = None,
        method: CustomHTTPMethod = CustomHTTPMethod.NONE,
        data: bytes = None,
        headers: dict[str, str] = None):
    custom_http_request: CustomHTTPRequest
    custom_http_request = CustomHTTPRequest(
        scheme=scheme.value,
        web_hook=web_hook,
        host=host,
        port=port.value,
        params=params)
    return send_custom_request_spec(custom_http_request, method=method, data=data, headers=headers)


@complete_configure_custom_logging("issue_statistics")
def main():
    custom_issues_id: CustomHTTPRequest = CustomHTTPRequest.issues_id_request()
    issues_id_response: str = send_custom_request_spec(
        custom_issues_id,
        method=CustomHTTPMethod["GET"],
        data=None,
        headers=auth_headers)
    logger.debug("Issues ID:")
    logger.debug(json.dumps(issues_id_response, ensure_ascii=False, indent=2))
    issue_parser: IssueResponseParser = IssueResponseParser(issues_id_response)
    issues_id: list[str] = issue_parser.get_issue_id()

    custom_assignees: CustomHTTPRequest = CustomHTTPRequest.assignees_request(issues_id)
    assignees_response: str = send_custom_request_spec(
        custom_assignees,
        method=CustomHTTPMethod["GET"],
        data=None,
        headers=auth_headers)
    logger.debug("Assignees:")
    logger.debug(json.dumps(assignees_response, ensure_ascii=False, indent=2))
    assignees_parser: AssigneesResponseParser = AssigneesResponseParser(assignees_response)
    youtrack_issues: list[YoutrackIssue] = assignees_parser.get_youtrack_issues()

    custom_issue_work_items: CustomHTTPRequest = CustomHTTPRequest.issue_work_items(issues_id)
    issue_work_items_response: str = send_custom_request_spec(
        custom_issue_work_items,
        method=CustomHTTPMethod["GET"],
        data=None,
        headers=auth_headers)
    logger.debug("IssueWorkItems:")
    logger.debug(json.dumps(issue_work_items_response, ensure_ascii=False, indent=2))
    issue_work_items_parser: IssueWorkItemParser = IssueWorkItemParser(issue_work_items_response)
    youtrack_issue_work_items: list[YoutrackIssueWorkItem] = issue_work_items_parser.get_issue_work_items()
    logger.error(f"len = {len(youtrack_issue_work_items)}")

    youtrack_info: YoutrackInfo = YoutrackInfo(youtrack_issues, youtrack_issue_work_items)
    results: list[YoutrackInfoRepresentation]
    results = [youtrack_info.prepare_youtrack_info_representation(issue) for issue in issues_id]

    logger.info("--------------------")
    for result in results:
        if result.issue is None:
            continue
        else:
            logger.info(result.repr())
            logger.info("--------------------")


if __name__ == "__main__":
    main()
