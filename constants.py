from datetime import date
from enum import Enum
from os import environ


class CustomPort(Enum):
    HTTP = 80
    HTTPS = 443
    NONE = None

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomScheme(Enum):
    HTTP = "http"
    HTTPS = "https"
    NONE = None

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomHTTPMethod(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    OPTIONS = "OPTIONS"
    NONE = None

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class RequiredAttributeMissing(BaseException):
    """Required attribute has no specified value."""


today: date = date.today()
current_year: int = today.year
current_month: int = today.month

if today.month == 1:
    start_date: date = date(today.year, today.month, 1)
else:
    start_date: date = date(today.year, today.month - 1, today.day)

host_name: str = "youtrack.protei.ru"
port_number: int = CustomPort["HTTPS"].value
scheme_protocol: str = CustomScheme["HTTPS"].value

logins_users: dict[str, str] = {
    "bochkova": "Бочкова Софья",
    "brovko": "Бровко Максим",
    "chursin": "Чурсин Алексей",
    "demyanenko": "Демьяненко Светлана",
    "fesenko": "Фесенко Виктор",
    "kuksina": "Куксина Ольга",
    "lyalina": "Лялина Кристина",
    "matyushina": "Мозглякова Вероника",
    "mazyarova": "Мазярова Дарья",
    "nigrej": "Нигрей Дмитрий",
    "nikitina": "Никитина Наталья",
    "sobolev-p": "Соболев Павел",
    "vykhodtsev": "Выходцев Алексей",
    "tarasov-a": "Тарасов Андрей"
}
_logins: tuple[str, ...] = tuple(logins_users.keys())
logins: str = ",".join(_logins)

_states_active: str = ",".join(("Active", "Paused", "Discuss", "Closed", "Done", "Review", "Test", "Verified"))

_projects: str = ",".join(("DOC", "DOC_ST", "ARCH", "ARCH_ST"))

auth_headers: dict[str, str] = {
    "Authorization": " ".join(("Bearer", environ.get("AUTH_TOKEN"))),
    "Accept": "application/json",
    "Content-Type": "application/json",
}
