import urllib.parse
from datetime import date
from typing import NamedTuple, Optional
from urllib.request import Request

from loguru import logger

from .constants import scheme_protocol, host_name, port_number, RequiredAttributeMissing, CustomScheme, CustomPort, \
    start_date, today, _states_active, _projects, logins
from .main import _protocol


class CustomHTTPRequest:
    def __init__(
            self, *,
            scheme: str = None,
            web_hook: str = None,
            host: str = None,
            port: int = None,
            params: tuple[tuple[str, str]] = None):
        if scheme is None:
            scheme = scheme_protocol
        if port is None:
            port = port_number
        self._scheme: str = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._port: int = port
        self._params = params

    def __str__(self):
        return f"{self.__class__.__name__}: scheme = {self.scheme}, web hook = {self.web_hook}, host = {self.host}, " \
               f"post = {self.port}, params = {self.params}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.scheme}, {self.web_hook}, {self.host}, {self.port}, {self.params})>"

    def __hash__(self):
        return hash(self.url())

    @classmethod
    def __call__(cls):
        return cls()

    def __len__(self):
        return len(self.params)

    def __bool__(self):
        return len(self) != 0

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.url() == other.url()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.url() != other.url()
        else:
            return NotImplemented

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, value):
        self._host = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = value

    @property
    def scheme(self):
        return self._scheme

    @scheme.setter
    def scheme(self, value):
        self._scheme = value

    @property
    def web_hook(self):
        return self._web_hook

    @web_hook.setter
    def web_hook(self, value):
        self._web_hook = value

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, value):
        self._params = value

    def _get_params(self):
        if self.params is None:
            return ""
        _params: str = urllib.parse.quote_plus(
            "&".join([f"{param_name}={param}" for param_name, param in self.params]), "=/&")
        return f"?{_params}"

    def _get_web_hook(self):
        if self.web_hook is None:
            logger.error("Web hook is a required attribute")
            raise RequiredAttributeMissing
        return self.web_hook.rstrip("/")

    def _get_scheme(self):
        if self.scheme is None:
            return CustomScheme[f"{_protocol}"].value
        return self.scheme.lower().strip("/")

    def _get_port(self):
        if self.port is None:
            return CustomPort[f"{_protocol}"].value
        return self.port

    def _get_host(self):
        if self.host is None:
            logger.error("Host is a required attribute")
            raise RequiredAttributeMissing
        return self.host.strip("/")

    def url(self):
        return f"{self._get_scheme()}://{self._get_host()}:{self._get_port()}/{self.web_hook}{self._get_params()}"

    @staticmethod
    def _period(from_date: date = start_date, end_date: date = today):
        return f"{from_date.isoformat()} .. {end_date.isoformat()}"

    @classmethod
    def issues_id_request(cls):
        scheme: str = scheme_protocol
        host: str = host_name
        port: int = port_number
        web_hook: str = "api/issues"
        query: str = " AND ".join(
            (f"State: {_states_active}",
             f"updated: {cls._period()}",
             "has: work",
             f"project: {_projects}",
             f"(Assignee: {logins})")
        )
        # logger.error(f"{cls._period()}")
        params: tuple[tuple[str, str], ...] = (
            ("$top", "-1"),
            ("fields", "idReadable"),
            ("query", query)
        )
        return cls(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params)

    @classmethod
    def assignees_request(cls, issues: list[str]):
        scheme: str = CustomScheme.HTTPS.value
        host: str = host_name
        port: int = port_number
        web_hook: str = "api/issues"
        query: str = " AND ".join(
            (f"issue ID: {','.join(issues)}",
             f"work author: {logins}")
        )
        params: tuple[tuple[str, str], ...] = (
            ("$top", "-1"),
            ("fields", "idReadable,summary,customFields(name,value,value(login),value(name),value(name,login))"),
            ("query", query),
            ("customFields", "Assignee"),
            ("customFields", "Исполнители_Doc"),
            ("customFields", "Исполнители_Doc_ST"),
            ("customFields", "Исполнители_Arch"),
            ("customFields", "Исполнители_Arch_ST")
        )
        return cls(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params)

    @classmethod
    def issue_work_items(cls, issues: list[str]):
        scheme: str = scheme_protocol
        host: str = host_name
        port: int = port_number
        web_hook: str = "api/workItems"

        query: str = f"issue ID: {','.join(issues)}"
        params: tuple[tuple[str, str], ...] = (
            ("$top", "-1"),
            ("fields", "author(login),duration(minutes),issue(idReadable)"),
            ("query", query)
        )
        return cls(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params)


class CustomPreparedRequest(NamedTuple):
    url: str
    data: bytes
    headers: dict[str, str]
    host: str
    unverifiable: bool
    method: str

    def __str__(self):
        return f"{self.__class__.__name__}: method = {self.method}, host = {self.host}\n" \
               f"headers = {[header for header in self.headers]}\n" \
               f"url = {self.url}\n" \
               f"data = \"{self.data}\""

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.url}, {self.data}, {[header for header in self.headers]}, " \
               f"{self.host}, {self.unverifiable}, {self.method})>"

    def __hash__(self):
        return hash((self.host, self.headers, self.data))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.to_request() == other.to_request()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.to_request() != other.to_request()
        else:
            return NotImplemented

    @classmethod
    def from_custom_request(
            cls, *,
            custom_http_request: CustomHTTPRequest,
            method: str = None,
            data: bytes = None,
            headers: dict[str, str] = None):
        if headers is None:
            headers = dict()
        return cls(
            custom_http_request.url(),
            data,
            headers,
            custom_http_request.host,
            False,
            method)

    def to_request(self):
        if self.headers is None:
            self.headers = dict()
        return Request(self.url, self.data, self.headers, self.host, self.unverifiable, self.method)


class CustomHTTPClient:
    def __init__(self, _custom_prepared_request: CustomPreparedRequest):
        self._custom_prepared_request: CustomPreparedRequest = _custom_prepared_request
        self._request: Optional[Request] = self.custom_prepared_request.to_request()

    def __str__(self):
        return f"{self.__class__.__name__}: custom http request = {self._custom_prepared_request}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._custom_prepared_request})>"

    @classmethod
    def __call__(cls):
        return cls(CustomPreparedRequest.from_custom_request(custom_http_request=CustomHTTPRequest()))

    @property
    def request(self):
        return self._request

    @request.setter
    def request(self, value):
        self._request = value

    @property
    def custom_prepared_request(self):
        return self._custom_prepared_request

    @custom_prepared_request.setter
    def custom_prepared_request(self, value):
        self._custom_prepared_request = value

    def get_response(self) -> str:
        with urllib.request.urlopen(self.request) as req:
            return req.read().decode("utf-8")
